package springhomework.demo.Controller.Rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springhomework.demo.Model.Book;
import springhomework.demo.Model.SearchModel;
import springhomework.demo.Service.BookService;
import springhomework.demo.Utility.Pagination;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
//@RequestMapping("/book")
public class BookRestController {

    @Autowired
    private BookService service;

    @GetMapping(value = "/book", params = "name")
    public Map<String, Object> getBook(@RequestParam String name){
        Map<String, Object> results = new HashMap<>();
        Book book = service.findByName(name);

        results.put("CODE", "200");
        results.put("Message", "Success");
        results.put("books", book);

        return results;
    }

    @GetMapping(value = "/book", params = "categoryid")
    public Map<String, Object> getBookByCategory(@RequestParam("categoryid") Long id){
        System.out.println("I am " + id);
        Map<String, Object> results = new HashMap<>();
        Book book = service.findByCategory(id);

        results.put("CODE", "200");
        results.put("Message", "Success");
        results.put("books", book);

        return results;
    }

//    @GetMapping("/book")
//    public Map<String, Object> getBook(){
//        Map<String, Object> results = new HashMap<>();
//        List<Book> books = service.getBook();
//
//        results.put("CODE", "200");
//        results.put("Message", "Success");
//        results.put("books", books);
//
//        return results;
//    }

    @PostMapping("/book")
    public Map<String, Object> insertBook(@RequestBody Book book){
        Map<String, Object> results = new HashMap<>();

        Book response = service.insert(book);

        results.put("CODE", "200");
        results.put("Message", "Created Successfully");
        results.put("book" , response);

        return results;
    }

    @DeleteMapping("/book/{id}")
    public Map<String, Object> deleteBook(@PathVariable long id, @RequestBody Book book){
        Map<String, Object> results = new HashMap<>();

        Book response = service.delete(id, book);

        results.put("CODE", "200");
        results.put("Message", "Deleted Successfully");
        results.put("book" , response);

        return results;
    }


    @PutMapping("/book/{id}")
    public Map<String, Object> updateBook(@PathVariable long id, @RequestBody Book book){
        Map<String, Object> results = new HashMap<>();


        Book response = service.update(id, book);

        results.put("CODE", "200");
        results.put("Message", "Updated Successfully");
        results.put("book" , response);

        return results;
    }

    @GetMapping(value = "/book")
    public Map<String, Object> select(@RequestParam(value = "page", required = true,defaultValue = "1")int page,
                                   @RequestParam(value = "limit",required = true,defaultValue = "4")int limit){
        Pagination pagination = new Pagination(page,limit);
        pagination.setPage(page);
        pagination.setLimit(limit);
        pagination.nextPage();
        pagination.previousPage();
        pagination.setTotalCount(service.getBookCount());
        pagination.setTotalPages(pagination.getTotalPages());

        Map<String, Object> response = new HashMap<>();
        List<Book> book = service.findAll(pagination);

        response.put("CODE", "200");
        response.put("Message", "Retrieve Data Successfully");
        response.put("pagination", pagination);
        response.put("book", book);
        return response;

    }

//    @GetMapping(value = "/book/categoryid={categoryid}/title={title}")
//    public Map<String, Object> getBookByCategoryAndName(@RequestParam SearchModel searchModel) {
//
//        System.out.println("customQuery = categoryid " + searchModel.getCategoryId());
//        System.out.println("customQuery = title " + searchModel.getBookName());
//        Map<String, Object> results = new HashMap<>();
//        Book book = service.findByCategoryAndName(searchModel.getCategoryId(), searchModel.getBookName());
//
//
//        results.put("CODE", "200");
//        results.put("Message", "Success");
//        results.put("books", book);
//
//        return results;
//    }

//    @RequestMapping(method = RequestMethod.GET, name = "/book")
//    public String controllerMethod(@RequestParam Map<String, String> customQuery) {
//
//        System.out.println("customQuery = categoryid " + customQuery.containsKey("categoryid"));
//        System.out.println("customQuery = title " + customQuery.containsKey("title"));
//        return customQuery.toString();
//    }

    @GetMapping("/book/{id}")
    public Map<String, Object> getBook(@PathVariable long id){
        Map<String, Object> results = new HashMap<>();
        Book book = service.findById(id);

        results.put("CODE", "200");
        results.put("Message", "Success");
        results.put("books", book);

        return results;
    }

}
