package springhomework.demo.Controller.Rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springhomework.demo.Model.Book;
import springhomework.demo.Model.Category;
import springhomework.demo.Service.CategoryService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/category")
public class CategoryRestController {
    @Autowired
    private CategoryService categoryService;

    @GetMapping()
    public Map<String, Object> getCategories(){
        Map<String, Object> results = new HashMap<>();
        List<Category> categories = categoryService.getCategory();

        results.put("CODE", "200");
        results.put("Message", "Success");
        results.put("categories", categories);

        return results;
    }

    @PostMapping()
    public Map<String, Object> insertCategory(@RequestBody Category category){
        Map<String, Object> results = new HashMap<>();

        Category response = categoryService.insert(category);

        results.put("CODE", "200");
        results.put("Message", "Created Successfully");
        results.put("categories" , response);

        return results;
    }

    @DeleteMapping("/{id}")
    public Map<String, Object> deleteCategory(@PathVariable long id){
        Map<String, Object> results = new HashMap<>();

        boolean response = categoryService.delete(id);

        results.put("CODE", "200");
        results.put("Message", "Deleted Successfully");
        results.put("isDeleted" , response);

        return results;
    }

    @PutMapping("/{id}")
    public Map<String, Object> updateCategory(@PathVariable long id, @RequestBody Category category){
        Map<String, Object> results = new HashMap<>();

        Category response = categoryService.update(id, category);
        results.put("CODE", "200");
        results.put("Message", "Updated Successfully");
        results.put("category" , response);

        return results;
    }
}
