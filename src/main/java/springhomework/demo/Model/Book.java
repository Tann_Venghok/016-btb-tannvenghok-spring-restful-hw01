package springhomework.demo.Model;

public class Book {
    private int id;
    private String title;
    private String author;
    private String description;
    private String thumbNail;
    private int categoryId;

    public Book(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getThumbNail() {
        return thumbNail;
    }

    public void setThumbNail(String thumbNail) {
        this.thumbNail = thumbNail;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public Book(int id, String title, String author, String description, String thumbNail, int categoryId) {
        this.id = id;
        this.title = title;
        this.author = author;
        this.description = description;
        this.thumbNail = thumbNail;
        this.categoryId = categoryId;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", description='" + description + '\'' +
                ", thumbNail='" + thumbNail + '\'' +
                ", categoryId=" + categoryId +
                '}';
    }
}
