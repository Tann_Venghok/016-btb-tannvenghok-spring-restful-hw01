package springhomework.demo.Model;

public class SearchModel {
    private long categoryId;
    private String bookName;

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(long categoryId) {
        this.categoryId = categoryId;
    }

    public SearchModel(long categoryId, String bookName) {
        this.categoryId = categoryId;
        this.bookName = bookName;
    }

    @Override
    public String toString() {
        return "SearchModel{" +
                "categoryId=" + categoryId +
                ", bookName='" + bookName + '\'' +
                '}';
    }
}
