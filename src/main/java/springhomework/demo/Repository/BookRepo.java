package springhomework.demo.Repository;

import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;
import springhomework.demo.Model.Book;
import springhomework.demo.Utility.Pagination;

import java.util.List;

@Repository
public interface BookRepo {

    @Insert("INSERT INTO tb_books ( title, author, description, thumbNail, category_id) VALUES( #{title}, #{author}," +
            "#{description}, #{thumbNail}, #{categoryId})")
    boolean insert(Book book);

    @Select("SELECT * FROM tb_books")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "thumbNail", column = "thumbnail"),
            @Result(property = "categoryId", column = "category_id")
    })
    List<Book> getBook();

    @Delete("DELETE FROM tb_books WHERE id = #{id}")
    boolean delete(long id, Book book);

    @Update("UPDATE tb_books SET " +
            "title =  #{title}," +
            "author = #{author}," +
            "description = #{description}, " +
            "thumbnail = #{thumbNail}," +
            "category_id = #{categoryId} WHERE id = #{id} ")
    boolean update(long id, String title, String author, String description, String thumbNail, long categoryId);

    @Select("SELECT * FROM tb_books WHERE id = #{id}")
    Book findById(long id);

    @Select("SELECT * FROM tb_books WHERE title = #{title}")
    Book findByName(String title);

    @Select("SELECT * FROM tb_books WHERE category_id = #{categoryId}")
    Book findByCategory(Long categoryId);

    @Select("SELECT * FROM tb_books ORDER BY id ASC LIMIT #{pagination.limit}  OFFSET #{pagination.offset}")
    List<Book> findAll(@Param("pagination") Pagination pagination);

    @Select("SELECT * FROM tb_books WHERE category_id = #{categoryId} AND title = #{title}")
    Book findByCategoryAndTitle(long categoryId, String title);

    @Select("SELECT count(*) FROM tb_books")
    int getCount();
}
