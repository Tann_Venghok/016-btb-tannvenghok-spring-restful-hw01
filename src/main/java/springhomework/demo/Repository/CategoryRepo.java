package springhomework.demo.Repository;

import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.DeleteMapping;
import springhomework.demo.Model.Book;
import springhomework.demo.Model.Category;

import java.util.List;

@Repository
public interface CategoryRepo {
    @Insert("INSERT INTO tb_categories (category_id, category_name) VALUES( #{categoryId}, #{categoryName})")
    boolean insert(Category category);

    @Select("SELECT * FROM tb_categories")
    @Results({
            @Result(property = "categoryId", column = "category_id"),
            @Result(property = "categoryName", column = "category_name"),
    })
    List<Category> getCategory();

    @Delete("DELETE FROM tb_categories WHERE category_id = #{id}")
    boolean delete(long id);

    @Update("Update tb_categories SET category_name = #{categoryName} " +
            " WHERE category_id = #{id}")
    boolean update(long id, String categoryName);

}
