package springhomework.demo.Service;


import springhomework.demo.Model.Book;
import springhomework.demo.Utility.Pagination;

import java.util.List;

public interface BookService {
    List<Book> getBook();

    Book insert(Book book);

    Book delete(long id, Book book1);

    Book update(long id, Book book);

    Book findById(long id);

    Book findByName(String title);

    List<Book> findAll(Pagination pagination);

    Book findByCategory(Long categoryId);

    Book findByCategoryAndName(Long categoryId, String title);

    int getBookCount();
}
