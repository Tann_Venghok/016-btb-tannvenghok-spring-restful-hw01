package springhomework.demo.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import springhomework.demo.Model.Book;
import springhomework.demo.Repository.BookRepo;
import springhomework.demo.Utility.Pagination;

import java.util.List;

@Service
public class BookServiceImp implements BookService {
    @Autowired
    private BookRepo bookRepo;

    @Override
    public List<Book> getBook() {
        return bookRepo.getBook();
    }

    @Override
    public Book insert(Book book) {
        boolean isInserted = bookRepo.insert(book);
        if(isInserted) {
            return book;
        }else{
            return null;
        }
    }

    @Override
    public Book delete(long id, Book book) {
        boolean isDeleted = bookRepo.delete(id, book);
        if(isDeleted){
            return book;
        }else{
            return null;
        }
    }

    @Override
    public Book update(long id, Book book) {
        boolean isUpdated = bookRepo.update(id, book.getTitle(), book.getAuthor(), book.getDescription(), book.getThumbNail(),
                book.getCategoryId());
        if(isUpdated)
            return book;
        else
            return null;
    }

    @Override
    public Book findById(long id) {
        Book book = bookRepo.findById(id);
        if(book == null)
            return null;
        else
            return book;
    }

    @Override
    public Book findByName(String title) {
        return bookRepo.findByName(title);
    }

    @Override
    public Book findByCategory(Long categoryId) {
        return bookRepo.findByCategory(categoryId);
    }

    @Override
    public Book findByCategoryAndName(Long categoryId, String title) {
        return bookRepo.findByCategoryAndTitle(categoryId, title);
    }

    @Override
    public int getBookCount() {
        return bookRepo.getCount();
    }

    @Override
    public List<Book> findAll(Pagination pagination) {
        return bookRepo.findAll(pagination);
    }
}
