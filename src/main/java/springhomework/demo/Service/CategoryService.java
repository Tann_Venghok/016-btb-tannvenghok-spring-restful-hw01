package springhomework.demo.Service;

import springhomework.demo.Model.Category;

import java.util.List;

public interface CategoryService {
    List<Category> getCategory();

    Category insert(Category category);

    boolean delete(long id);

    Category update(long id, Category category);
}
