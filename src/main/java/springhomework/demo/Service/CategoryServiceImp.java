package springhomework.demo.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import springhomework.demo.Model.Category;
import springhomework.demo.Repository.CategoryRepo;

import java.util.List;

@Service
public class CategoryServiceImp implements CategoryService{
    @Autowired
    private CategoryRepo categoryRepo;
    @Override
    public List<Category> getCategory() {
        return categoryRepo.getCategory();
    }

    @Override
    public Category insert(Category category) {
        boolean isInserted = categoryRepo.insert(category);
        if(isInserted) {
            return category;
        }else{
            return null;
        }
    }

    @Override
    public boolean delete(long id) {
        return categoryRepo.delete(id);
    }

    @Override
    public Category update(long id, Category category) {
//        return categoryRepo.update(id, category.getCategoryName());
        boolean isUpdated = categoryRepo.update(id, category.getCategoryName());
        if(isUpdated){
            return category;
        }else
            return null;
    }

}
